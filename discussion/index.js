/* NOTES:
document - refers to the whole webpage

to access objects in the document, you may use:
    document.getElementById('txt-first-name')
        or
    document.querySelector('#txt-first-name')
        or
    document.getElementsByClassName('txt-inputs')
        or
    document.getElementByTagName('input')
*/


//access the objects needed
const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');


//call out the variable
//addEventListener('method or the event or action')
//event parameter is triggered, what will happen next?
txtFirstName.addEventListener('keyup', (event) => {
    //innerHTML displays/adds an object in the document
    //to access the value from the firstname.
    spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
});

txtFirstName.addEventListener('keyup', (event) => {
    console.log(event.target);
    console.log(event.target.value);
});

txtLastName.addEventListener('keyup', (event) => {
    //innerHTML displays/adds an object in the document
    //to access the value from the firstname.
    spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
});


/*
const updateFullName = () => {
    let firsName = txtFirstName.value;
    let lastName -txtLastName.value;

    spanFullName.innerHTML = `${firstName} ${lastName}`
}

txtFirstName.addEventListener('keyup', updateFullName);
txtLastName.addEventListener('keyup', updateFullName);

*/